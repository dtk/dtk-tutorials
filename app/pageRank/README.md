DTK Distributed tutorial: PageRank
======================

Overview
--------

Write a pagerank algorithm (iterative method) using dtk distributed data structures.

PageRank is an algorithm used by Google to rank websites in their search engine results. It works by counting the number and quality of links to a page to determine a rough estimate of how important the website is. The underlying assumption is that more important websites are likely to receive more links from other websites.
PageRank outputs a probability distribution used to represent the likelihood that a person randomly clicking on links will arrive at any particular page.

* Entry: a directed graph (N vertices)
* Solution: stationnary distribution of all pages (vertices)

Parameter: damping_factor d

Initial value: P=1/N for all pages

Step n: P_i = (1-d)/N + d * sum of probabilities of all pages P_j linking to i at step (n-1) divided by number of links  in P_j


* Input of the program:
 graph file (metis format)

* Output:
 solution vector file

Requirements
------------

- Qt-5.3 or more
- cmake 2.8.12 or up
- dtk git
- dtk-linear-algebra-sparse git
- mpi3 compatible library (optionnal) e.g. openmpi-1.8.5


Add Qt5 bin dir in your PATH

Download & Compile dtk
------------

~~~
git clone https://github.com/d-tk/dtk.git
cd dtk
mkdir build
cd build
cmake ..
make -j4
~~~

Download & Compile dtk-linear-algebra-sparse
------------

~~~
git clone https://github.com/d-tk/dtk-linear-algebra-sparse.git
cd dtk
mkdir build
cd build
cmake ..
make -j4
~~~

~~~
# add config path
echo "[linear-algebra-sparse]" > ~/.config/inria/dtk-linear-algebra-sparse.ini
echo "plugins=XXXX/dtk-linear-algebra-sparse/build/plugins" >> ~/.config/inria/dtk-linear-algebra-sparse.ini
# add config path for dtkdistributed
echo "[communicator]" > ~/.config/inria/dtk-distributed.ini
echo "plugins=XXXX/dtk-plugins-distributed/build/plugins" >> ~/.config/inria/dtk-distributed.ini
~~~

Create your application
----------------------

### first step: cmakefile ###


~~~
project(pageRank)

cmake_minimum_required(VERSION 2.8.12)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(CMAKE_AUTOMOC ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

find_package(dtk 1.0.0)
find_package(dtkLinearAlgebraSparse 0.1.1)

set(${PROJECT_NAME}HEADERS
  PageRank.h )

set(${PROJECT_NAME}_SOURCES
  PageRank.cpp
  main.cpp)

add_executable(${PROJECT_NAME}
  ${${PROJECT_NAME}_SOURCES}
  ${${PROJECT_NAME}_HEADERS})

target_link_libraries(${PROJECT_NAME}
  dtkCore
  dtkDistributed
  dtkLinearAlgebraSparse
 )

target_link_libraries(${PROJECT_NAME} Qt5::Core)
target_link_libraries(${PROJECT_NAME} Qt5::Network)
~~~

### then create the main skeleton ###

~~~
#include <QtCore>
int main(int argc, char **argv)
{
    qDebug() << "hello world";
}
~~~


###  check that everything compiles ###

~~~
mkdir build
cmake ..
make
./pageRank
hello world
~~~

###  create a dtk distributed application ###

TIP: As an example, you can check dtkDistributedSlave in dtk/app

* instantiate a dtkDistributedApplication object

~~~
  dtkDistributedApplication *app = dtkDistributed::create(argc, argv);
  ...
~~~

* add custom parameters (filename):

~~~
  QCommandLineParser *parser = app->parser();
  ...
~~~

* Initialize application

* Initialiaze dtk-linear-algebra-sparse layer:

~~~
    dtkLinearAlgebraSparse::pluginManager::initialize();
~~~



###  create the main task (QRunnable) to be executed ###

Should have members to set the file path to read the graph

1. Create the dtkDistributedTopology graph from the graph file
2. Create a solution vector (dtkDistributedVectorData class)

3. Write the solution

~~~
class mainTask: public QRunnable
{
public:
    double damping_factor = 0.15;
    double epsilon = 1.e-8;
    QString filename;
    QString vectorImpl = "dtkDistributedVectorData";
    QString sol_vector_file;
public:
    void run(void) {
        dtkDistributedGraphTopology graph;
        graph.read(filename,dtkDistributedGraphTopology::MetisDirectedFormat);
        dtkVector<double> *solution = dtkLinearAlgebraSparse::vectorFactory::create<double>(vectorImpl);
    }
  };
~~~

#### exec the main task (QRunnable) in main: ####

need to call:

1. spawn
2. exec
3. unspawn

###  create the pageRank class (QRunnable) in separate PageRank.h|cpp files ###

API:

~~~
#pragma once
#include <QRunnable>
class PageRankPrivate;
class dtkDistributedGraphTopology;
template <typename T> class dtkVector;
class PageRank: public QRunnable
{
public:
             PageRank(void);
    virtual ~PageRank(void);
public:
    void setGraph(dtkDistributedGraphTopology *matrix);
    void setSolution(dtkVector<double> *sol_vector);
    void setDampingFactor(double d) ;
    void setEpsilon(double e) ;
public:
    void run(void);
private:
    PageRankPrivate *d;
};
~~~

### check using the given data:

graph defined like this:
* first line is: *#vertices* *#edges*
* each line: *edge* from current line to *number* (id starts at 1)

~~~
6 21
 1 2 3 4 5 6
 2 3 4 5 6
 3 4 5 6
 4 5 6
 5 6
 6
~~~

solution:
p = [4.3254; 0.6488;  0.3731; 0.2674; 0.2106; 0.1748]

### run the program

You can choose the number of processes, the communicator plugin policy, etc.
