/* @(#)PageRank.h ---
 *
 * Author: Nicolas Niclausse
 * Copyright (C) 2015 - Nicolas Niclausse, Inria.
 * Created: 2015/05/12 14:14:22
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#pragma once

#include <QRunnable>

class PageRankPrivate;
class dtkDistributedGraphTopology;
template <typename T> class dtkVector;

class PageRank: public QRunnable
{
public:
             PageRank(void);
    virtual ~PageRank(void);

public:
    void setGraph(dtkDistributedGraphTopology *matrix);
    void setSolution(dtkVector<double> *sol_vector);
    void setDampingFactor(double d) ;
    void setEpsilon(double e) ;

public:
    void run(void);

private:
    PageRankPrivate *d;
};
