/* @(#)main.cpp ---
 *
 * Author: Nicolas Niclausse
 * Copyright (C) 2014 - Nicolas Niclausse, Inria.
 * Created: 2014/04/25 16:28:19
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#include <dtkDistributed>
#include <dtkLinearAlgebraSparse>
#include <dtkLog>

#include <QtCore>
#include "PageRank.h"

class pageRankTask: public QRunnable
{
public:
    double damping_factor = 0.15;
    double epsilon = 1.e-7;
    QString filename;
    QString vectorImpl = "dtkDistributedVectorData";
    QString sol_vector_file;

public:
    void run(void) {
        dtkDistributedGraphTopology graph;
        QTime timer; timer.start();
        dtkDistributedCommunicator *comm = dtkDistributed::communicator::instance();
        if (!graph.read(filename,dtkDistributedGraphTopology::MetisDirectedFormat)) {
            dtkError()<< "can't read file " << filename;
            return;
        }

        dtkVector<double> *solution = dtkLinearAlgebraSparse::vectorFactory::create<double>(vectorImpl);
        PageRank pagerank;
        solution->resize(graph.vertexCount());
        pagerank.setGraph(&graph);
        pagerank.setSolution(solution);
        pagerank.setDampingFactor(damping_factor);
        pagerank.setEpsilon(epsilon);

        comm->barrier();
        pagerank.run();

        if (comm->rank() == 0) {
            qDebug() << "pagerank duration:" << timer.elapsed() << "ms";
            qDebug() << "stationnary distribution: " ;
            if (!sol_vector_file.isEmpty()) {
                solution->writeMatrixMarket(sol_vector_file);
            }
            qDebug()<<  solution->at(0) << solution->at(1) << solution->at(2)<< solution->at(3) ;
        }
        comm->barrier();
        delete solution;
    }
};

int main(int argc, char **argv)
{
    dtkDistributedApplication *app = dtkDistributed::create(argc, argv);
    app->setApplicationName("dtkDistributedSlave");
    app->setApplicationVersion("1.0.0");
    app->setOrganizationName("inria");

    QCommandLineParser *parser = app->parser();
    parser->setApplicationDescription("DTK PageRank.");

    QCommandLineOption fileOption("file", "graph file", "filename");
    parser->addOption(fileOption);

    QCommandLineOption solOption("sol", "solution vector to be written", "file");
    parser->addOption(solOption);

    app->initialize();

    // ------------ initialize layers
    dtkLinearAlgebraSparse::pluginManager::initialize();

     if (!parser->isSet(fileOption)) {
         qCritical() << "Error: no graph file specified ! Use --file <filename> " ;
         return 1;
     }


    pageRankTask work;
    work.filename = parser->value(fileOption);
    work.sol_vector_file = parser->value(solOption);

    app->spawn();
    app->exec(&work);
    app->unspawn();

    return 0;
}
