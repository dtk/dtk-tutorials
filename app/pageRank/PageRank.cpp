/* @(#)PageRank.cpp ---
 *
 * Author: Nicolas Niclausse
 * Copyright (C) 2015 - Nicolas Niclausse, Inria.
 * Created: 2015/05/12 14:19:35
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */


#include "PageRank.h"
#include <dtkLinearAlgebraSparse>
#include <dtkDistributed>

class PageRankPrivate
{
public:
    double damping_factor;
    double epsilon;
    dtkVector<double> *sol;
    dtkDistributedGraphTopology *graph;
};


PageRank::PageRank() : d(new PageRankPrivate)
{
    d->sol   = NULL;
    d->graph = NULL;
}

PageRank::~PageRank(void)
{
    delete d;
}

void PageRank::run(void)
{
    dtkVector<double> tmp(*d->sol);
    dtkVector<double> linkCount(*d->sol);

    dtkVector<double>& sol = *(d->sol);
    dtkDistributedGraphTopology& graph = *(d->graph);

    dtkDistributedGraphTopology::iterator it  = graph.cbegin();
    dtkDistributedGraphTopology::iterator end = graph.cend();

    double init = 1.0/d->graph->vertexCount();
    qlonglong i,j;

    linkCount.fill(0);
    for(;it != end; ++it) {
        i = it.id();
        dtkDistributedGraphTopology::Neighbours n = graph[i];
        sol[i] = init;

        dtkDistributedGraphTopology::Neighbours::iterator nit  = n.begin();
        dtkDistributedGraphTopology::Neighbours::iterator nend = n.end();

        for(;nit != nend; ++nit) {
            j = *nit;
            linkCount[j] += 1;
        }
    }
    bool stop_criterion = false;
    double current_p;
    qlonglong iteration = 0;
    qlonglong max_iterations = 200;
    while(!stop_criterion) {
        tmp = sol;
        iteration ++;
        it  = d->graph->cbegin();

        for(;it != end; ++it) {
            i = it.id();
            dtkDistributedGraphTopology::Neighbours n = (*d->graph)[i];
            dtkDistributedGraphTopology::Neighbours::iterator nit  = n.begin();
            dtkDistributedGraphTopology::Neighbours::iterator nend = n.end();
            double current_p = 0;

            for(;nit != nend; ++nit) {
                j = *nit;
                current_p += tmp[j] / linkCount[j];
            }
            current_p *= (1 - d->damping_factor);
            sol[i] = d->damping_factor +  current_p;
        }
        tmp -= sol;
        double epsilon = tmp.asum();
        // qDebug() <<  "iteration" << iteration << epsilon;
        if (epsilon < d->epsilon || iteration > max_iterations) {
            qDebug() << "stop at iteration" << iteration;
            stop_criterion = true;
        }
        sol.clearCache();
        tmp.clearCache();
    }
}

void PageRank::setEpsilon(double e)
{
    d->epsilon = e;
}

void PageRank::setGraph(dtkDistributedGraphTopology *graph)
{
    d->graph = graph;
}

void PageRank::setSolution(dtkVector<double> *sol)
{
    d->sol = sol;
}

void PageRank::setDampingFactor(double factor)
{
    d->damping_factor = factor;
}
